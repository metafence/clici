#!/usr/bin/env bash

# make sure script exits with first error
set -e

# test case
export PREPARE_TEST_VAR="some value that shouldnt get transported"

# make sure pip is installed
assert_available pip

mkdir -p $ARTIFACTS_ROOT || true

assert_exported PHASE_PARAMS

# to get rid of lingering exit codes from the commands above, at the end of the tasks script,
# always exit with exit 0.
# this is important, since with exit codes != 0, clici assumes an error in the phase

exit 0
