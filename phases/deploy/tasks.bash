#!/usr/bin/env bash

# make sure script exits with first error
set -e

rm -rf public || true

mkdir -pv $ARTIFACTS_ROOT/docs/files/${TARGET_RELEASE}
cp -v $ARTIFACTS_ROOT/clici $ARTIFACTS_ROOT/docs/files/${TARGET_RELEASE}

mkdir $ARTIFACTS_ROOT/docs/files/latest
cp $ARTIFACTS_ROOT/clici $ARTIFACTS_ROOT/docs/files/latest

ls artifacts/docs
cp -r $ARTIFACTS_ROOT/docs public

# to get rid of lingering exit codes from the commands above, at the end of the tasks script,
# always exit with exit 0.
# this is important, since with exit codes != 0, clici assumes an error in the phase
exit 0
