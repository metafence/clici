#!/usr/bin/env bash

# make sure script exits with first error
set -e

unset errors

# run tests of subcalls as a basis for the others
call_clici $STARTUP_DIR/src/test/subcalls positive-tests || (echo "FAILURE! Positive Test failed!"; errors=true; exit 1)
call_clici $STARTUP_DIR/src/test/subcalls negative-test && (echo "FAILURE! negative tests passed!"; errors=true; exit 1)

# run simple positive phase sequence tests
# prepare: make sure the first runs as solo phase
# itest: fair ground in the middle with some phases missing between prepare and stest
# deploy: the last phase in sequence
# detached_phase: a phase not part of the sequence

declare -a tests=("prepare" "itest" "deploy" "detached_phase")
for test in  "${tests[@]}"; do
    call_clici $STARTUP_DIR/src/test/phase_sequence $test || (echo "FAILURE! Test $test failed!"; errors=true; exit 1)
done

# run simple negative phase sequence tests
declare -a tests=("negative_fail" "negative_on_success_fail" "negative_on_error_fail")
for test in  "${tests[@]}"; do
    call_clici $STARTUP_DIR/src/test/phase_sequence $test && (echo "FAILURE! Test $test succeeded!"; errors=true; exit 1)
done




[[ -z $errors ]] && exit 0

exit 1