#!/usr/bin/env bash

# finally script must run as far as possible, so *disable* error handling here
set +e

# restore target version settings
# export TARGET_VERSION=$OLD_TARGET_VERSION
# export TARGET_VERSION_MAJOR=$OLD_TARGET_VERSION_MAJOR
# export TARGET_VERSION_MINOR=$OLD_TARGET_VERSION_MINOR
# export TARGET_VERSION_BUILD=$OLD_TARGET_VERSION_BUILD

# do fancy stuff ;-)
