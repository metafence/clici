#!/usr/bin/env bash

# make sure script exits with first error
set -e

# check wether we have changes in the working directory

[[ $(git diff) ]] && echo Cannot release - changes in working directory && exit 1

RELEASE_TAG=V${TARGET_VERSION}

echo preparing release $RELEASE_TAG ...

mkdir -p $ARTIFACTS_ROOT/release/$RELEASE_TAG

cp -v $ARTIFACTS_ROOT/clici $ARTIFACTS_ROOT/release/$RELEASE_TAG/
cp -r $ARTIFACTS_ROOT/docs   $ARTIFACTS_ROOT/release/$RELEASE_TAG/

echo "creating downloadable archives ..."

assert_available tar

mkdir -p $ARTIFACTS_ROOT/release/$RELEASE_TAG/downloads

tar cvzf $ARTIFACTS_ROOT/release/$RELEASE_TAG/downloads/clici-$RELEASE_TAG-doc.tar.gz -C $ARTIFACTS_ROOT docs
tar cvzf $ARTIFACTS_ROOT/release/$RELEASE_TAG/downloads/clici-$RELEASE_TAG.tar.gz -C $ARTIFACTS_ROOT clici

# to get rid of lingering exit codes from the commands above, at the end of script,
# always exit with exit 0.
# this is important, since with exit codes != 0, clici assumes an error in the phase
exit 0
