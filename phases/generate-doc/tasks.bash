#!/usr/bin/env bash

# make sure script exits with first error
set -e

pip install mkdocs

assert_available mkdocs

# create slate documentation
mkdir -p ${ARTIFACTS_ROOT}/docs

echo creating pages
pip install -r ${PHASE_DIR}/mkdocs_requirements.txt


# copy over release notes, if present
if [[ -f ${ARTIFACTS_ROOT}/ReleaseNotes_latest.md ]] ; then 
    cp ${ARTIFACTS_ROOT}/ReleaseNotes_latest.md src/main/docs/ReleaseNotes.md
fi

# create site
mkdocs build --strict

# to get rid of lingering exit codes from the commands above, at the end of the tasks script,
# always exit with exit 0.
# this is important, since with exit codes != 0, clici assumes an error in the phase
exit 0
