#! /bin/bash

# make sure script exits with first error
set -e

# make sure gitlab token is there - if not running in gitlab_ci
assert_exported GITLAB_TOKEN
assert_available jq
assert_available envsubst

echo $SHELL

echo "
Generate release file
"

function query_feature_details () {

    set -e

    FEATURE_ID="${1:1}"

    DETAILS_JSON=$(curl -H "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/issues?iids[]=$FEATURE_ID")

    [[ $DETAILS_JSON =~ ^.*"message".*$  ]] && echo $DETAILS_JSON | jq '.message' && exit 1 
    
    
    export FEATURE_TITLE=$(echo $DETAILS_JSON | jq -r '.[0].title')
    export FEATURE_DESCRIPTION=$(echo $DETAILS_JSON | jq -r '.[0].description')
}

#
# header

RELEASE_NOTES_FILE=${ARTIFACTS_ROOT}/ReleaseNotes_${TARGET_VERSION}.md
export RELEASE_DATE=$(date +"%e.%m.%Y %T")

cat $PHASE_DIR/release_notes/header_template.md | envsubst > $RELEASE_NOTES_FILE

# collect all commits until the last crossing with tag $PROD_TAG_NAME
# tag $PROD_TAG_NAME is moving and points to current commit in production
# see ../env.source for current tag

# easiest way to let bash split at line ending instead of spaces

IFS=$'\n' 
PROCESSED_FEATURES=''

for LOG_MESSAGE in $(git rev-list --all ${GIT_COMMIT_ID} ^${LAST_RELEASE_TAG} --format=oneline); do
    if [[ "$LOG_MESSAGE" =~ ^.*#[0-1]+.*$ ]]; then 
        export FEATURE_ID=$(echo $LOG_MESSAGE | cut -d ' ' -f 2)

        # feature already processed?
        [[ "$PROCESSED_FEATURES" =~ ^.*${FEATURE_ID}.*$ ]] && echo "multiple occurence of ${FEATURE_ID} ignored" && continue
        PROCESSED_FEATURES="$PROCESSED_FEATURES ${FEATURE_ID}"

        query_feature_details $FEATURE_ID
        cat $PHASE_DIR/release_notes/feature_template.md | envsubst >> $RELEASE_NOTES_FILE
    else
        [[ -z $IGNORE_LEADING_COMMITS ]] && [[ -z $FEATURE_ID ]] && echo there are commits not covered by a feature - you cannot release && exit 1
    fi
done

# copy release notes also to version "latest"
cp $RELEASE_NOTES_FILE $ARTIFACTS_ROOT/ReleaseNotes_latest.md

# to get rid of lingering exit codes from the commands above, at the end of the tasks script,
# always exit with exit 0.
# this is important, since with exit codes != 0, clici assumes an error in the phase
exit 0
