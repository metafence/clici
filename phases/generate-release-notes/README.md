# generate release notes

creates a release notes file with the features developed on since last release.

find release notes in artifacts directory
gitlab: find release notes at the artifacts section of pipeline

## requirements

- a git tag PROD pointing to the commit of last release, and having a commit path to the branch you're creating your release notes from
- jq, envsubst, curl, clici installed

## configuration

the script is configured  by environment settings (see $PHASES_DIR/env.source for sane defaults):

- *GITLAB_TOKEN* : an API access token to be able to read feature titles and descriptions from gitlab issues

- *LAST_RELEASE_TAG* : the name of the git tag of last release (default: PROD). Used to create a delta between the last release and current state of branch.

- *IGNORE_LEADING_COMMITS* : Normally, the most current commit on the branch must be a commit naming a feature id in its message. By setting this variable, this check can be disabled.

## noteworthy candy

- when a feature gets named again in a commit message, new release notes automatically contain that feature as well.

- when creating release notes prematurely on release branch or develop, put __export IGNORE_LEADING_COMMITS="yes"__ to your local_env.source.

- feature title and description is taken unchanged from thei ticketing system; so, to format your feature description, just use markdown. If you want to create sub sections, use level 4 and below directly in the description text.

## shortcomings

- currently moving the PROD tag needs a force push, which sometimes is not desirable. Using proper release tags or querying  gitlab releases api might be other  approaches worth exploring.