
# clici

**Clean build pipelines with bash**

---

## Overview

**"Do One Thing and Do It Well"** - Unix Philosophy

clici is a straightforward and powerful bash-based build framework designed for simplicity and flexibility:

- Use **real bash scripts**, incorporating all features like piping, in-place documentation, environment variables, sed, grep, gawk, and more.
- Test your CI/CD pipelines locally.
- CI agnostic: Works with GitLab, Jenkins, Bamboo, Raspberry Pi, and others.
- OS agnostic: Runs anywhere bash is available.
- Additive scaffolding: Grows with your project needs.

## Basic Usage

clici operates as a bash script providing a command-line interface for managing your build environments.

```bash
> clici status
git version 2.27.0
GIT_BRANCH=develop
SCRIPT_PATH=./clici
SCRIPT_DIR=/home/dude/projects/metafence/clici
STARTUP_DIR=/home/dude/projects/metafence/clici
PHASES_ROOT=./phases
ARTIFACTS_ROOT=/home/dude/projects/metafence/clici/artifacts
USER=dude
```

### Initialization and Scaffolding

Use **init** to set up your project and scaffold phases:

```bash
/home/dude> mkdir -p projects/test
/home/dude> cd projects/test
/home/dude/projects/test> clici init prepare build deploy
git version 2.27.0
GIT_BRANCH=
mkdir: created directory './phases'
mkdir: created directory '/home/dude/projects/test/artifacts'
mkdir: created directory './phases/prepare'
mkdir: created directory './phases/build'
mkdir: created directory './phases/deploy'

/home/dude/projects/test> tree

.
├── artifacts
└── phases
    ├── build
    │   ├── finally.bash
    │   ├── on_error.bash
    │   ├── on_okay.bash
    │   └── tasks.bash
    ├── deploy
    │   ├── finally.bash
    │   ├── on_error.bash
    │   ├── on_okay.bash
    │   └── tasks.bash
    ├── env.source
    └── prepare
        ├── finally.bash
        ├── on_error.bash
        ├── on_okay.bash
        └── tasks.bash
```

### Starting the Build

```bash
/home/dude/projects/test> clici deploy
git version 2.27.0
GIT_BRANCH=
-----------------------------------------------------------
processing /home/dude/projects/test/phases/prepare @ piggy ...
-----------------------------------------------------------
phase 'prepare' passed
-----------------------------------------------------------
processing /home/dude/projects/test/phases/build @ piggy ...
-----------------------------------------------------------
phase 'build' passed
-----------------------------------------------------------
processing /home/dude/projects/test/phases/deploy @ piggy ...
-----------------------------------------------------------
phase 'deploy' passed
target phase 'deploy' reached.
```

### Getting Help

Execute the help command to discover more functionalities:

```bash
> clici help
```