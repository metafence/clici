
# Variables and Functions

Like in any other CI tool, clici provides some useful environment variables:

```bash
touch ${ARTIFACTS_ROOT}/${PHASE}
```

## Build Related

- **PHASE**: Name of the current phase. For a complete list of phases see [phases](phases.md).

- **PHASE_DIR**: Path to the file directory of the current phase, where `tasks.bash` resides.

- **PHASE_PARAMS**: Parameters used at execution; for example:

```bash
clici stest One two three
```

then **PHASE_PARAMS** equals "One two three". You still have to split the string if you're interested in processing parameters separately.

- **ARTIFACTS_ROOT**: Directory for intermediary files and other artifacts of the build.

- **PHASES_ROOT**: Root directory of phases directory structure.

- **TARGET_VERSION**: The target version to be built. See [Versioning](versioning.md) for more.

- **CLICI_IMPRINT_PROJECT_ROOT**: If set, the value of `PHASES_ROOT` is embedded statically in the result of [command line completion](completion.md). This means command line completion will find the commands in the directory it was at the time of script generation, regardless of where or how `clici` is executed later.


## Git Related

- **GIT_PATH**: The path to the git executable. Undefined if git is unavailable.

- **GIT_BRANCH**: The name of the git branch the script currently is on.

- **GIT_COMMIT_ID**: The full-length commit id of the current codebase. "Undefined" if git is not available.

## Clici Related

- **SCRIPT_FILE**: File name of the script currently running (clici).

- **SCRIPT_PATH**: Path of the clici script currently running.

- **CLICI_CURRENT_SCRIPT**: Absolute path to the current script file running, e.g., clici, some env.source, tasks.bash, on_error whatsoever. Should be $0 but when sourcing it's not. This is limited to files clici knows of.

- **CLICI_SILENCED**: Set this to reduce output of clici a little bit.

- **SCRIPT_DIR**: The directory the clici script is located in.

- **STARTUP_DIR**: The original working directory. Just in case you don't trust $PWD in the middle of a build.

- **CLICI_VERSION**: Version of clici.

- **PHASE_SEQUENCE**: Sequence of build phases, normally "prepare patch generate build utest itest release atest stage stest deploy". Changing this for an existing build might break your build.

- **IN_PHASE_SEQUENCE** (read-only): Whether or not a script runs inside the defined phase sequence, like prepare, or deploy, or so. If so, the env var is defined. If not, it's undefined.

- **IS_END_PHASE** (read-only): Whether or not the current phase is the last one in the sequence. If so, the env var is defined. If not, it's undefined. Always true (hence defined) for out-of-sequence commands.

- **CLICI_EXPERIMENTAL**: Enables experimental features like run_phase.

## Docker Related

- **DOCKER_REGISTRY_URL**: URL to the docker registry.

- **DOCKER_REGISTRY_USER**: User to use with the above docker registry.

- **DOCKER_REGISTRY_PASS**: Password to use with the above docker registry user.

## CI Server Related (see integrations)

- **ONLINE**: Can access some parts of the public internet.

- **ON_PUBLIC_CLOUD**: When set, then the current run happens on a public cloud service like gitlab.com.

- **PIPELINE_TYPE**: If running in a pipeline, then of which technology. (currently one of GitLab_CI or Azure).

## Some Useful Default Environment Variables

- **USER**: Name of the user running the clici script.

## Predefined Bash Functions

When marked with "(experimental)", then the function only is available if the CLICI_EXPERIMENTAL flag (see above) is set.

- **assert_available $1**: Make sure the program $1 is in your path. When the tool is not available, the run is stopped.

```bash
assert_available python
```

- **assert_exported $1**: Make sure the specified variable (only Name, omit Dollar sign) is exported. It still can be defined yet empty.

- **clean_artifacts**: Removes and recreates $ARTIFACTS_ROOT.

- **call_clici**: Start another clici in another project dir. First argument is the project root, all other arguments are commands and parameters.

- **install_clici**: Install the current running clici in the given path. You rarely should be in need of calling that. Is the only line if you create a custom install phase. Must be root (userid=0) to execute.

- **log_trace $1**: Print $1 plus current phases directory to the error output. $1 is optional. This is usually needed when debugging code and removed afterwards.

- **reload_env** (experimental): Load env vars from $PHASES_ROOT/env.source as well as $ENV_DIRECTORY/env.source and $ENV_DIRECTORY/secrets.source. This is automatically done when starting clici and before each phase, so there rarely should be a need to call this manually.

- **run_phase** (experimental): Runs another phase. No loop checks yet, so only call out-of-sequence phases here.

## Example: Make sure python is available

In ./phases/prepare/tasks.bash:

```bash
# make sure python is available
assert_available python
```

## Example: cascade clici instances for testing or submoduling:

In ./phases/stest/tasks.bash, call a test case implemented as another instance of clici:

```bash
call_clici $STARTUP_DIR/src/test/smoketests all
```

See more examples in the [Cook Book](cookbook.md)