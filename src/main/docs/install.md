# Installation


- First, <a href="../files/latest/clici" download type="application/octet-stream">Click to Download clici script</a>

- give executive rights
```
> chmod +x clici
```

- Optional: Use __sudo clici install__ to install/update:

```bash
> sudo ./clici install
git version 2.27.0
GIT_BRANCH=develop
'./clici' -> '/usr/local/bin/clici'
-rwxr-xr-x 1 root root 7259  5. Sep 07:11 /usr/local/bin/clici
```
