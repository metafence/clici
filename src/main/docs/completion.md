
# Command Line Interface Completions

This document provides details on how to configure and use shell completions for `clici`. Currently, support is provided for Zsh, with the potential to add Bash and Fish in the future.

## Zsh Completion

The `zshcompletion` command in `clici` generates a script that enables command-line completion for Zsh. This feature assists users by providing automatic suggestions for `clici` commands and options.

### Usage

To generate the Zsh completion script:

```bash
clici zshcompletion [path-to-completion-file] [completion-command]
```

Then, source in your .zshrc

### Parameters

- **path-to-completion-file**: Optional. Specifies the path where the completion script will be saved. If not provided, the script will default to `${ARTIFACTS_ROOT}/clici-completion.zsh`.
- **completion-command**: Optional. Specifies the command for which the completion is generated. If not provided, defaults to `clici`. This is useful if using a wrapper around clici.

### Environment Variables

- **CLICI_IMPRINT_PROJECT_ROOT**: If set, the value of `PHASES_ROOT` is embedded statically in the result of [command line completion](completion.md). This means command line completion will find the commands in the directory it was at the time of script generation, regardless of where or how `clici` is executed later.


### Integration

To use the generated completion script, source it in your `.zshrc` file:

```bash
source /path/to/artifacts/clici-completion.zsh
```

Ensure that the path points to where the script was saved. After sourcing the script, Zsh will provide tab completions for `clici` commands.

### Considerations

- Ensure that `PHASES_ROOT` is correctly set before generating the completion script, especially if `CLICI_IMPRINT_PHASES_ROOT` is used.
- The completion script needs to be regenerated if the commands or structure of `clici` change.

## Bash Completion

_Coming soon..._

## Fish Completion

_Coming soon..._