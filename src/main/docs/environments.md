
# Environments

Manage environment-specific settings and files effectively by utilizing designated environment directories within clici.

## Default Environment

By default, clici operates under the assumption that it is in a local environment, implying that it is neither running in a pipeline nor hosted on a CI server. You can confirm the current environment by checking the `ENVIRONMENT` setting in the output of the `clici status` command:

```bash
> clici status
git version 2.25.1
COMMAND=status
...
STARTUP_DIR=/home/micwin/projects/clici
ENVIRONMENT='local'
ENV_DIRECTORY='/home/micwin/projects/clici/environments/local'
...
```

## Managing Environment Variables

For environment-specific variables, or to map variables depending on the selected environment, utilize an environment-specific `env.source` file:

```bash
> cat ./environments/local/env.source
export DUMMY_ENVIRONMENT_VAR=isibisi
```

## Handling Secrets

To manage sensitive information, such as tokens, use a `secrets.source` file within the respective environment directory:

```bash
> cat ./environments/local/secrets.source
export GITLAB_TOKEN=xxxxxxxxxxxxxxxxxxx
```

**Important:** Ensure that `secrets.source` is listed in your `.gitignore` to prevent sensitive data from being tracked in version control.

## Utilizing Environment Variables in Scripts

Within your `tasks.bash` scripts, reference files from your environment-specific directory using the `ENV_DIRECTORY` environment variable:

```bash
# Example usage within tasks.bash
source ${ENV_DIRECTORY}/env.source
```

## Switching Environments

To switch to a different environment, set the `ENVIRONMENT` variable to your desired context *before* executing clici commands:

```bash
> export ENVIRONMENT=something
> clici status
...
STARTUP_DIR=/home/micwin/projects/clici
ENVIRONMENT='something'
ENV_DIRECTORY='/home/micwin/projects/clici/environments/something'
...
```

Typically, the `local` environment suffices for development, but you can configure the `ENVIRONMENT` in your CI server's pipeline settings to adapt to different stages like testing, staging, or production.