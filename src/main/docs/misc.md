# clean

Primarily, removes and recreates directory $ARTIFACTS_DIR

```
» clici clean
git version 2.25.1
COMMAND=clean
GIT_BRANCH=develop
GIT_COMMIT_ID=f806fa4aeb4467144ccc3f84fc279a0dd102571f
TARGET_VERSION=new
CLICI_VERSION=0.8.237709948
removed '/home/micwin/projects/clici/artifacts/clici'
removed '/home/micwin/projects/clici/artifacts/docs/phases/index.html'
removed directory '/home/micwin/projects/clici/artifacts/docs/phases'
removed '/home/micwin/projects/clici/artifacts/docs/css/theme.css'
removed '/home/micwin/projects/clici/artifacts/docs/css/theme_extra.css'
removed directory '/home/micwin/projects/clici/artifacts/docs/css'
removed '/home/micwin/projects/clici/artifacts/docs/predefined/index.html'
removed directory '/home/micwin/projects/clici/artifacts/docs/predefined'
removed '/home/micwin/projects/clici/artifacts/docs/license/index.html'

...

removed '/home/micwin/projects/clici/artifacts/docs/img/favicon.ico'
removed directory '/home/micwin/projects/clici/artifacts/docs/img'
removed '/home/micwin/projects/clici/artifacts/docs/fonts/fontawesome-webfont.woff2'
removed '/home/micwin/projects/clici/artifacts/docs/fonts/fontawesome-webfont.woff'
mkdir: created directory '/home/micwin/projects/clici/artifacts'
done
```

See [cookbook](cookbook.md) for further tricks with clean.

