# Build Phases

A **phase** represents a discrete execution of a specified stage within the build process. Each phase may include up to four optional Bash scripts:

- **tasks.bash**: Executes specific tasks within a subshell.
- **on_error.bash**: Triggers if `tasks.bash` exits with an error.
- **on_okay.bash**: Executes if `tasks.bash` exits without an error.
- **finally.bash**: Always runs at the end of a phase, useful for cleanup tasks.

While all scripts are optional, a phase is generally expected to contain at least a `tasks.bash` to be functional.

## Execution Order and Phases

clici processes the following phases, though not all projects will require every phase:

1. **prepare**: Validates or sets up the environment.
2. **patch**: Applies environment-specific adjustments to files.
3. **generate**: Produces source files from templates.
4. **build**: Compiles the source into artifacts.
5. **utest**: Conducts unit testing.
6. **itest**: Handles integration testing.
7. **release**: Manages the release of build candidates.
8. **atest**: Performs automated acceptance testing.
9. **stage**: Deploys to a staging environment.
10. **stest**: Runs system tests or smoke tests, which are more extensive than integration tests.
11. **deploy**: Deploys documentation and binaries to their respective locations or artifact stores.

Each phase maps to a clici command and is associated with a dedicated directory that contains the scripts `tasks.bash`, `on_okay.bash`, and `on_error.bash`.

### Default Phase Execution

For example, the default procedure for the **itest** phase includes:

- Executing all preceding phases.
- Sourcing environment variables from `$PHASES_ROOT/env.source` if available.
- Resetting the working directory to `$STARTUP_DIR` and running `itest/tasks.bash`.
  - On success: Executes `itest/on_okay.bash` and `itest/finally.bash` if they exist.
  - On failure: Runs `itest/on_error.bash` and `finally.bash`, then halts further phase execution.
- Cleans up with `finally.bash` regardless of outcome.
- Proceeds to the next phase or concludes if no further tasks are pending.

The working directory reverts to its original state between phases. Build intermediates or artifacts are stored in the `ARTIFACTS_ROOT` environment variable.

## Global Configuration

Configure the build environment globally by populating `${PHASES_ROOT}/env.source` with custom environment variables. Note: Exiting `env.source` with `exit` will terminate the entire build process; use `return` to exit the script without stopping the build.

Environment variables set in `tasks.bash`, `on_error.bash`, `on_okay.bash`, and `finally.bash` are isolated to their respective phases. To persist data across phases, store it in `${ARTIFACTS_ROOT}`:

```bash
echo "built by $USER" > $ARTIFACTS_ROOT/package
```


## Customizing Phase Order

Modify the build sequence by setting the `PHASE_SEQUENCE` environment variable, specifying phase names separated by spaces:

```bash
export PHASE_SEQUENCE='first second third' 
clici third
```

Set `PHASE_SEQUENCE` before initiating the build, through an export command, a prefix, or within `${PHASES_ROOT}/env.source`. Changes to `PHASE_SEQUENCE` after the build has started will not affect the current sequence.

The recommended way of adjusting the phase sequence is editing `${PHASES_ROOT}/env.source`.

