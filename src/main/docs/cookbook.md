
# Cookbook

## Basic Workflow

- Add basic structure with a phase named "build" by using scaffolding:

```bash
> clici init build

mkdir: created directory './phases'
mkdir: created directory './artifacts'
mkdir: created directory './phases/build'

> tree
.
├── artifacts
└── phases
    ├── build
    │   ├── on_error.bash
    │   ├── on_okay.bash
    │   └── tasks.bash
    └── env.source
```

- Add some custom commands:

```bash
> vim phases/build/tasks.bash
# (click click click) ESC:w!
> cat phases/build/tasks.bash
#!/usr/bin/env bash

# make sure script exits with the first error
set -e

# do fancy stuff
echo "Doing fancy stuff"

# to get rid of lingering exit codes from the commands above, at the end of the tasks script,
# always exit with exit 0.
# this is important, since with exit codes != 0, clici assumes an error in the phase
exit 0
```

- Execute the first run:

```bash
> clici build

#-----------------------------------------------------------
processing /path/to/phases/build @ piggy ...
#-----------------------------------------------------------
Doing fancy stuff
phase build reached.
```

- To add another phase, just call init again with the new phase. 
For instance, to add a system test phase:

```bash
> clici init stest
mkdir: created directory './phases/stest'

> tree
.
├── artifacts
└── phases
    ├── build
    │   ├── on_error.bash
    │   ├── on_okay.bash
    │   └── tasks.bash
    ├── env.source
    └── stest
        ├── on_error.bash
        ├── on_okay.bash
        └── tasks.bash

4 directories, 7 files
```

To get a full list of phases, consult [phases.md](phases.md).

## Full Blown Build Structure

You can create a build containing all the phases by stating:

```bash
> clici init --all

mkdir: created directory './phases'
mkdir: created directory '/path/to/artifacts'
mkdir: created directory './phases/prepare'
mkdir: created directory './phases/patch'
mkdir: created directory './phases/generate'
mkdir: created directory './phases/build'
mkdir: created directory './phases/utest'
mkdir: created directory './phases/itest'
mkdir: created directory './phases/release'
mkdir: created directory './phases/atest'
mkdir: created directory './phases/stage'
mkdir: created directory './phases/stest'
mkdir: created directory './phases/deploy'
```

This will result in the following directory tree:

```bash
> tree

.
├── artifacts
└── phases
    ├── atest
    │   ├── on_error.bash
    │   ├── on_okay.bash
    │   └── tasks.bash
    ├── build
    │   ├── on_error.bash
    │   ├── on_okay.bash
    │   └── tasks.bash
    ├── deploy
    │   ├── on_error.bash
    │   ├── on_okay.bash
    │   └── tasks.bash
    ├── env.source
    ├── generate
    │   ├── on_error.bash
    │   ├── on_okay.bash
    │   └── tasks.bash
    ├── itest
    │   ├── on_error.bash
    │   ├── on_okay.bash
    │   └── tasks.bash
    ├── patch
    │   ├── on_error.bash
    │   ├── on_okay.bash
    │   └── tasks.bash
    ├── prepare
    │   ├── on_error.bash
    │   ├── on_okay.bash
    │   └── tasks.bash
    ├── release
    │   ├── on_error.bash
    │   ├── on_okay.bash
    │   └── tasks.bash
    ├── stage
    │   ├── on_error.bash
    │   ├── on_okay.bash
    │   └── tasks.bash
    ├── stest
    │   ├── on_error.bash
    │   ├── on_okay.bash
    │   └── tasks.bash
    └── utest
        ├── on_error.bash
        ├── on_okay.bash
        └── tasks.bash
```

### Notes

- The order _tree_ provides is **not** the order in which phases and scripts will execute.
 
- You rarely should be in need of using --all phases. If you do so, learn about the KISS principle. If you still do - you have my regrets, you poor soul :-)

- You also can use that command to fill up missing phases when there already is a structure in place. 

## Custom Commands

You can create specific commands:

```bash
> clici init snafu

mkdir: created directory './phases/snafu'

> clici snafu
-----------------------------------------------------------
processing ./phases/snafu @ somewhere ...
-----------------------------------------------------------
phase 'snafu' passed
done
```

Note that such a command is not integrated in the standard phase sequence but executed standalone.

## Custom Clean Phase

You can modify what happens when cleaning.

To do that, modify the clean command by converting it into a phase script:

```bash
» clici init clean
git version 2.25.1
COMMAND=init
PHASE_PARAMS=clean
GIT_BRANCH=develop
GIT_COMMIT_ID=f806fa4aeb4467144ccc3f84fc279a0dd102571f
TARGET_VERSION=new
CLICI_VERSION=0.8.237709948
mkdir: created directory './phases/clean'

```

The generated _$PHASES_ROOT/clean/tasks.bash_:

```bash
#!/usr/bin/env bash

# make sure script exits with first error
set -e

# clean and recreate artifacts dir
clean_artifacts

# to get rid of lingering exit codes from the commands above, at the end of the tasks script,
# always exit with exit 0.
# this is important, since with exit codes != 0, clici assumes an error in the phase
exit 0

```

As you see, the former clean phase is called by the function _clean_artifacts_. If you want to remove and recreate your artifacts directory by yourself, just remove that code.

If you want to extend the clean phase with more code, for instance to clean up a Kubernetes namespace or else, just add that code before _clean_artifacts_.

You have the hooks _on_error.bash_ and _on_okay.bash_ as well, so you can decide what to do afterwards, or in case of an error.

Remember that the clean phase is not part of the build workflow, so doesn't get called automatically.

## Custom Install Phase

You also can overwrite the install phase:

```bash
> clici init install
```

This generates the following __$PHASES_ROOT/install/tasks.bash__:

```bash
#!/usr/bin/env bash

# make sure script exits with first error
set -e

# install clici
install_clici

# to get rid of lingering exit codes from the commands above, at the end of the script,
# always exit with exit 0.
# this is important, since with exit codes != 0, clici assumes an error in the phase
exit 0

```

as well as the standard __on_error.bash__, __on_okay.bash__ and __finally.bash__

## Clean on Every Run

If you want to call clean on every build, then call _clean_artifacts_ in your first phase:

./phases/prepare/tasks.bash:

```bash
#!/usr/bin/env bash

# make sure script exits with first error
set -e

# make sure pip is installed
assert_available pip

clean_artifacts

# to get rid of lingering exit codes from the commands above, at the end of the tasks script,
# always exit with exit 0.
# this is important, since with exit codes != 0, clici assumes an error in the phase
exit 0

```

These will not execute the custom clean phase, though, only the default deletion and recreation of artifacts.

## Clean on Every Run Using $PHASES_ROOT/environments/local/secrets.source

If you like a more non-persistent approach, temporarily add the following line as the last line of your $PHASES_ROOT/environments/local/secrets.source:

```bash
export PHASE_SEQUENCE="clean $PHASE_SEQUENCE"
```

Since secrets.source is in .gitignore, the phase sequence stays the same for the rest of your team.