# Versioning

The version of  artifacts in build is kept in the environment variable TARGET_VERSION. It consists of three digits, separated by a dot:

- __TARGET_VERSION_MAJOR__ : donating the architectural, overall version

- __TARGET_VERSION_MINOR__ : the second digit - between first and second dot, donating the functional version. If a Major version gains functionality, this version rises.

- __TARGET_VERSION_BUILD__ : the build of the artifact. Normally, this should rise too and hence being growing; but nowadays this holds a pipeline id, or a commit hash of underlying sources.

When undefined, TARGET_VERSION_MAJOR and TARGET_VERSION_MINOR result in 0, with TARGET_VERSION_BUILD being set to $GIT_COMMIT_ID. This leads to a TARGET_VERSION = 0.0.$GIT_COMMIT_ID.

There are multiple ways to change TARGET_VERSION:

- set it directly (not recommended):

> export TARGET_VERSION=1.0.0

- set major version only: 

> export TARGET_VERSION_MAJOR=1

which results in TARGET_VERSION=1.0.$GIT_COMMIT_ID

- set minor version only (not recommended): 

> export TARGET_VERSION_MINOR=1

which results in TARGET_VERSION=0.1.$GIT_COMMIT_ID

- set both, major and minor

> export TARGET_VERSION_MAJOR=1 
> export TARGET_VERSION_MINOR=1

which results in TARGET_VERSION=1.1.$GIT_COMMIT_ID

Since these are semantically bound to the source code, you might want to set major and minor in $PHASES_ROOT/env.source directly, committing them with the source code, whilst TARGET_VERSION_BUILD is automatically set either by clici to be the commit id, or, if running in a pipeline, you might configure something like:

> export TARGET_VERSION_BUILD=$PIPELINE_ID

or similar in the pipeline code, or a coresponding build variable.

When doing a minor release, you surely want to define 

> export TARGET_VERSION_BUILD=RC1

to mark this build being a release candidate or so.

In a CD-Envionment, you might keep it undefined, so it points to the commit id its based upon.

