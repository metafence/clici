# Integrations

Due to its very basic requirements, clici integrates 
easily with any ci server out there; the point is - 
you would have to write boilerplate code to transfer 
enviromnent variables from one world to another - for each
new repository! So we decided to do some of that stuff 
for you. Here the list of currently and natively supported
CI servers and environment variables:

## GitLab-CI

- __GIT_BRANCH__ = __$CI_COMMIT_REF_NAME__ 

- __PIPELINE_TYPE__ = 'GitLab_CI' 

- if __$CI_SERVER_HOST" == "gitlab.com"__, then __ON_PUBLIC_CLOUD=true__ and __ONLINE=true__

## Azure

- __GIT_BRANCH__ = __$(Build.SourceBranchName)__

- __PIPELINE_TYPE__ ='Azure'

- __ONLINE__ = true

As you see, public cloud detection is not yet implemented