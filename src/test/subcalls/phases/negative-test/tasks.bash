#!/usr/bin/env bash

# make sure script exits with first error
set -e

# subcall negative test - just fail

echo "Failing on purpose"
exit 1