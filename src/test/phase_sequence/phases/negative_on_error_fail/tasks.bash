#!/usr/bin/env bash


# make sure script exits with first error
set -e

echo Failing on purpose on $CLICI_CURRENT_SCRIPT
exit 1

# do fancy stuff ;-)

# to get rid of lingering exit codes from the commands above, at the end of script,
# always exit with exit 0.
# this is important, since with exit codes != 0, clici assumes an error in the phase
exit 0
