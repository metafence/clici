#!/usr/bin/env bash


# make sure script exits with first error
set -e

echo This code should not execute - failing to indicate error in $CLICI_CURRENT_SCRIPT
exit 3



# do fancy stuff ;-)

# to get rid of lingering exit codes from the commands above, at the end of script,
# always exit with exit 0.
# this is important, since with exit codes != 0, clici assumes an error in the phase
exit 0
